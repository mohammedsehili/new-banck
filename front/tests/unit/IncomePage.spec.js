import { shallowMount } from '@vue/test-utils'
import IncomePage from '@/components/IncomePage/IncomePage.vue'

const finishAsyncTasks = () => new Promise(setImmediate)

test('si funciona la function ContactList ', async () => {
    const MockApiCall = jest.fn()
    const mockContacts = [
        {
            id: '01',
            name: 'Diegos',
            surname: 'ajami',
            email: 'Diegos.my@Diegos.com',
            mobile: '631125578',
        },
        {
            id: '02',
            name: 'diego',
            surname: 'espinosa',
            email: 'diego.esp@Diegos.com',
            mobile: '63254789',
        },
    ]
    MockApiCall.mockReturnValue(mockContacts)
    const wrapper = shallowMount(IncomePage, {
        methods: {
            displayMovements: MockApiCall,
        },
    })

    await finishAsyncTasks()
    expect(MockApiCall).toHaveBeenCalled()
})

test.skip('Este el Button de anadir ', async () => {
    const contact = {
        name: 'paco',
        surname: 'porras',
        mail: 'pantalones',
        telephone: '666666666',
        descrption: 'celiacos',
        date: '2020-05-12',
        time: '5:35 PM',
        people: 10,
        id: 1,
    }

    const mockGetcontact = jest.fn()
    mockGetcontact.mockResolvedValue(contact)
    console.log('contactos', mockGetcontact.mockResolvedValue(contact))

    const wrapper = shallowMount(IncomePage, {
        methods: {
            addContact: mockGetcontact,
        },
    })

    await finishAsyncTasks() // magia para esperar el click, la llamada y todo lo async

    expect(mockGetcontact).toHaveBeenCalled()
})
