import { shallowMount } from "@vue/test-utils";
import MovementList from "@/components/MovementPage/MovementList.vue";

const finishAsyncTasks = () => new Promise(setImmediate);

test("pinta la lista  limpia", () => {
  const wrapper = shallowMount(MovementList, {
    propsData: {
      ArrayMovement: [],
    },
  });
  const ArrayMovement = wrapper.findAllComponents(MovementList).wrappers;

  expect(ArrayMovement.length).toBe(1);
});

test("pinta el objeto recogido", () => {
  const ArrayMovement = [
    {
      id_Movements: "1",
      Name_Movements: "Compra de Verduras",
      Mony_Movements: "20€",
      Type_Movements: "Gastos",
      Date_Movements: "20/06/2020",
      Description_Movements: "Platanos,aguacates, verduras en general",
      Category_Movements: "Compra Semanal",
    },
  ];
  const wrapper = shallowMount(ArrayMovement, {
    propsData: {
      listMovement: ArrayMovement,
    },
  });
  const MovementItem = wrapper.findAllComponents(MovementList).wrappers;
  expect(MovementItem.length).toBe(0);
});
