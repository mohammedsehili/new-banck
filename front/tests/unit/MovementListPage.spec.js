import { shallowMount } from "@vue/test-utils";
import MovementListPage from "@/components/MovementPage/MovementListPage.vue";
import MovementItem from "@/components/MovementPage/MovementItem.vue";

test("pinta la lista vacia", () => {
  const wrapper = shallowMount(MovementListPage, {
    propsData: {
      movements: [],
    },
  });
  const movementItem = wrapper.findAll(MovementItem).wrappers;

  expect(movementItem.length).toBe(0);
});

test("muestra todos los movimientos de la lista", () => {
  const movement = [
    {
      id: "567",
      name: "Ignacio",
      money: "20€",
      type: "gastos",
      date: "20/06/20",
      description: "foodspring proteinas",
      category: "alimentacion",
    },
  ];
  const wrapper = shallowMount(MovementListPage, {
    propsData: {
      movements: movement,
    },
  });
  const MovementItem = wrapper.findAll(MovementItem).wrappers;
  expect(MovementItem.length).toBe(1);
});
