import { shallowMount } from '@vue/test-utils'
import MovementList from '@/components/MovementPage/MovementList.vue'
import MovementItem from '@/components/MovementPage/MovementItem.vue'

test.skip('pinta todo list de Movementos', () => {
    const wrapper = shallowMount(MovementItem, {
        propsData: {
            movement: [],
        },
    })
    const tableMovement = wrapper.findAll(MovementList).wrappers

    expect(tableMovement.length).toBe(0)
})

test.skip('Este el Button de Eliminar ', async () => {
    const movement = {
        idMovements: '1',
        nameMovements: 'test',
        monyMovements: '20',
        typeMovements: 'G',
        dateMovements: '2020-01-01',
        descriptionMovements: 'test1',
        categoryMovements: 'ropa',
    }
    const wrapper = shallowMount(MovementItem, {
        propsData: { movement: movement },
    })
    expect(wrapper.emittedByOrder()).toEqual([])

    const button = wrapper.find('.btndelete')
    button.trigger('click')
    await wrapper.vm.$nextTick()

    console.log('emmitedByOrder', wrapper.emittedByOrder())
    expect(wrapper.emittedByOrder()).toEqual([
        { name: 'button-delete', args: [movement.idMovements] },
    ])
})
