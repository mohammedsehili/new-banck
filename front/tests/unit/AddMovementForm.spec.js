import { shallowMount } from "@vue/test-utils";
import AddMovementForm from "@/components/AddAmountPage/AddMovementForm.vue";

test("Comprobamos que el componente recoge el listado de las categorias ", () => {
  const movementListCategory = [
    {
      id_category: "01",
      name_category: "ropa",
    },
    {
      id_category: "02",
      name_category: "comida",
    },
  ];

  const wrapper = shallowMount(AddMovementForm, {
    propsData: { movementListCategory: movementListCategory },
  });

  expect(wrapper.vm.movementListCategory).toEqual(movementListCategory);
});

test("Comprobamos que el componente pinta el listado de las categorias en el selector ", () => {
  const movementListCategory = [
    {
      id_category: "01",
      name_category: "ropa",
    },
    {
      id_category: "02",
      name_category: "comida",
    },
  ];

  const wrapper = shallowMount(AddMovementForm, {
    propsData: { movementListCategory: movementListCategory },
  });
  const optionListCategories = wrapper.findAll(".optionCategory");

  expect(optionListCategories.length).toBe(movementListCategory.length);
  console.log("Hola unai", optionListCategories[0]);
  //expect(optionListCategories[0]).toBe("ropa");
  //expect(optionListCategories[1]).toBe("comida");
});

test("Comprobamos que emitimos el evento con el enviamos el objeto movimiento ", async () => {
  const movementListCategory = [
    {
      id_category: "01",
      name_category: "ropa",
    },
    {
      id_category: "02",
      name_category: "comida",
    },
  ];

  const wrapper = shallowMount(AddMovementForm, {
    propsData: { movementListCategory: movementListCategory },
  });
  const inputName = wrapper.find(".name-movements");
  const inputMoney = wrapper.find(".money-movements");
  const inputDescription = wrapper.find(".description-movements");
  /* wrapper
    .findAll(".optionCategory")
    .at(1)
    .trigger("change");*/
  await wrapper.vm.$nextTick();

  inputName.setValue("Azucar");
  inputMoney.setValue("0.99");
  inputDescription.setValue("1 kilo");
  // inputCategory.setValue("comida");
  const movement = {
    idMovements: null,
    Name_Movements: "Azucar",
    Money_Movements: "0.99",
    Description_Movements: "1 kilo",
    Category_Movements: null,
    Type_Movements: null,
    Date_Movements: null,
  };
  expect(wrapper.vm.movement).toEqual(movement);
  const button = wrapper.find(".ButtonAdd");
  button.trigger("click");
  await wrapper.vm.$nextTick();
  expect(wrapper.emitted()).toEqual(
    {
      "new-movements": [[movement]],
    },
  );
});
