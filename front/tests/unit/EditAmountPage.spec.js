import { shallowMount } from '@vue/test-utils'
import EditAmountPage from '@/components/EditAmountPage/EditAmountPage.vue'
import IncomePage from '@/components/IncomePage/IncomePage.vue'

const finishAsyncTasks = () => new Promise(setImmediate)

test.skip('el componente recive lo que tiene que recivir', async () => {
    const MockApiCall = jest.fn()
    const mockContacts = [
        {
            name: 'Diegos',
            amount: 'ajami',
            description: 'Diegos.my@Diegos.com',
            category: 'Income',
        },
    ]
    MockApiCall.mockReturnValue(mockContacts)
    const wrapper = shallowMount(EditAmountPage, {
        methods: {
            ...IncomePage.methods,
            displayMovements: MockApiCall,
        },
    })
    const buttonEdit = wrapper.find('.button-edit')
    await wrapper.vm.$nextTick()
    await finishAsyncTasks()
    expect(MockApiCall).toHaveBeenCalled()
    expect(wrapper.vm.movements).toBe(mockContacts)
})
