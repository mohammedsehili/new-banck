import Vue from 'vue'
import App from '../src/App.vue'
import router from './routes'
import api from './api'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'

Vue.use(api)
Vue.config.productionTip = false

new Vue({
    render: h => h(App),
    router,
}).$mount('#app')
