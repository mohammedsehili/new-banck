import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
    routes: [
        {
            path: '/bb',
            component: function(resolve) {
                require(['@/components/LoginPage/LoginPage.vue'], resolve)
            },
        },
        {
            path: '/Register',
            component: function(resolve) {
                require(['@/components/LoginPage/RegisterPage.vue'], resolve)
            },
        },

        {
            path: '/',
            component: function(resolve) {
                require(['@/components/Layout/Navegation.vue'], resolve)
            },
        },
        {
            path: '/Home',
            component: function(resolve) {
                require(['@/components/Home/HomePage.vue'], resolve)
            },
        },

        {
            path: '/Income',
            component: function(resolve) {
                require(['@/components/IncomePage/IncomePage.vue'], resolve)
            },
        },
        {
            path: '/Expenses',
            component: function(resolve) {
                require(['@/components/ExpensesPage/ExpensesPage.vue'], resolve)
            },
        },
        {
            path: '/Movement',
            component: function(resolve) {
                require(['@/components/Movements/MovementList.vue'], resolve)
            },
        },

        {
            path: '/Add/:type',
            component: function(resolve) {
                require([
                    '@/components/AddAmountPage/AddMovementPage.vue',
                ], resolve)
            },
        },

        {
            path: '/Edit/:id_Movements',
            component: function(resolve) {
                require([
                    '@/components/EditAmountPage/EditMovementPage.vue',
                ], resolve)
            },
        },
    ],
})
export default router
