const root = '/api'

import { v4 as uuidv4 } from 'uuid'

export default {
    async _fetch(method, endpoint, data, headers) {
        const result = await fetch(endpoint, {
            method: method,
            body: JSON.stringify(data),
            headers: headers,
        })
        return await result.json()
    },
    async getExpenses(movements) {
        await fetch(`${root}/Movements`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id_movements: uuidv4(),
                name_movements: movements.name_movements,
                money_movements: movements.money_movements,
                type_movements: movements.type_movements,
                date_movements: new Date().toLocaleString(),
                description_movements: movements.description_movements,
                id_category: movements.category_movements,
            }),
        }).then(response => response.json())
    },
    async deleteMovements(id_movements) {
        await fetch(`/api/deleteMovements/` + id_movements, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            },
        }).then(response => response.json())
    },

    async displayApiCategory() {
        let result = await fetch(`${root}/displayCategory`)
        console.log('zior3')
        return await result.json()
    },

    async displayIMovements() {
        let result = await fetch(`${root}/desplayIMovements`)
        return await result.json()
    },
    async editdisplayaMovement(id_movements) {
        const result = await fetch(
            `${root}/editdisplayaMovement/` + id_movements
        )
        return await result.json()
    },
    async displayMovement() {
        let result = await fetch(`${root}/desplayMovement`)
        return await result.json()
    },

    async updateMovement(id_movements, localmovements) {
        await fetch(`${root}/updateMovement/` + id_movements, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                name_movements: localmovements.name_movements,
                money_movements: localmovements.money_movements,
                description_movements: localmovements.description_movements,
            }),
        }).then(response => response.json())
    },
}
