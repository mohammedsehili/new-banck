<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use Illuminate\Support\Facades\DB;

class CategoryTest extends TestCase
{

    /*************************tabla de categoria****************** */
    protected function setUp(): void
    {
        parent::setUp();
        DB::unprepared("
            CREATE TABLE  category (
                id_category	TEXT NOT NULL,
                name_category	TEXT
               
            );
            INSERT INTO category VALUES ('1','Alimentacion');
            INSERT INTO category VALUES ('2','Gastos Hogar');
        ");
    }


/********************Test de Mostrar Ctegoria ********************* */

public function testGetCtegory()
    {
           $this->withoutExceptionHandling();
        $this->json('GET', 'api/displayCategory')
            ->assertStatus(200)
            ->assertJson([
                [
                    'id_category' => '1',
                    'name_category' => 'Alimentacion',
                    
                   
                ]
                ,
                [
                    'id_category' => '2',
                    'name_category' => 'Gastos Hogar',
                ],
                
                ]);
    }











}