<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use Illuminate\Support\Facades\DB;

class MovementTest extends TestCase
{
/******************table de Movementos********** */
    protected function setUp(): void
    {
        parent::setUp();
        DB::unprepared("
            CREATE TABLE  Movements (
                id_Movements	TEXT NOT NULL,
                Name_Movements	text,
                Mony_Movements	text,
                Type_Movements	TEXT,
                Date_Movements	date,
                Description_Movements	TEXT,
                Category_Movements TEXT
            );
            INSERT INTO Movements VALUES ('1','Azucar', '20','G','2020-01-01','test1','comida');
            INSERT INTO Movements VALUES ('2','pantalon', '30','I','2020-02-01','test2','Ropa');
        ");
        // $this->withoutExceptionHandling();
    }

   
   
//---------------------Test de Mostrar Gastos Movements------------------//
public function testGetGMovements()
{
    $this->json('GET', 'api/desplayGMovements')
        ->assertStatus(200)
        ->assertJson([
            [
                'id_Movements' => '1',
                'Name_Movements' => 'Azucar',
                'Mony_Movements' => '20',
                'Type_Movements' => 'G',
                'Date_Movements' => '2020-01-01',
                'Description_Movements' => 'test1',
                'Category_Movements'=>'comida',
            ]
            ]
        );
}
/********************Test de Mostrar ingresos Movements********************* */
public function testGetIMovements()
{
    $this->json('GET', 'api/desplayIMovements')
        ->assertStatus(200)
        ->assertJson([
            [
                'id_Movements' => '2',
                'Name_Movements' => 'pantalon',
                'Mony_Movements' => '30',
                'Type_Movements' => 'I',
                'Date_Movements' => '2020-02-01',
                'Description_Movements' => 'test2',
                'Category_Movements'=>'Ropa',
            ]
            ]
        );
}





//-----------------Test de Eliminar Movements ---------------//

public function testDeleteMovements()
    {
       // $this->json('GET', 'api/deleteMovements/1')->assertStatus(200);

        $this->json('DELETE', 'api/deleteMovements/1')->assertStatus(200);

      //  $this->json('GET', 'api/deleteMovements/1')->assertStatus(404);
    }







}